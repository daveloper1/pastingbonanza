﻿using Microsoft.AspNetCore.Mvc; // support for MVC components
using PastingBonanza.Models;    // need our data models
using System.Linq;              // allows for database operations

/// <summary>
/// This project is Copyright 2022 - Dave Loper
/// All code was written by Dave Loper using Visual Studio Community 2019
/// Licenced via AGPL 3.0 for all original code. https://www.gnu.org/licenses/agpl-3.0.en.html
/// </summary>

namespace PastingBonanza.Controllers    // our namespace
{                                       // open our namespace
    public class HomeController : Controller    // our main home controller
    {                                           // open our main home controller
        private PasteContext context;                               // let's define our PasteContext
        public HomeController(PasteContext ctx) => context = ctx;   // and setup a constructor for our data which we will use here

        /// <summary>
        /// This is our main webpage which has an action including
        ///  - some database housecleaning
        ///  - and pull the list of our model into our view
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Index(string id)                   // open our IAction and work on the id string
        {                                                       // open our method
            PasteViewModel model = new PasteViewModel();        // setup our data model
            IQueryable<Paste> query = context.Contents;         // get content from our database

            // First we need to clean house before returning our data. TODO - Turn this into its own method and call it
            model.Pastes = query.OrderBy(t => t.Id).ToList();   // enumerate our full list of database objects
            int count = model.Pastes.Count;                     // count the objects
            int i = count - 10;                                 // hardcode our site to retain ten items
            foreach (var removeid in query)                     // iterate over our objects
            {                                                   // open iteration
                if ( i > 0)                                     // conditional (we will remove the oldest, leaving 10
                {                                               // open conditional
                    context.Contents.Remove(removeid);          // remove excess
                    i--;                                        // decrement to preserve the 10 items
                }                                               // open conditional
            }                                                   // close conditional
            context.SaveChanges();                              // commit db changes

            // Next, let's get the remaining data and return it
            model.Pastes = query.OrderBy(t => t.Id).ToList();   // pull the list of items in the db
            return View(model);                                 // return the model
        }                                                       // close our method

        [HttpPost]
        /// <summary>
        /// This method adds a new item to the database
        /// </summary>
        /// <returns></returns>
        public IActionResult Add(PasteViewModel model)      // use the viewmodel to push our data
        {                                                   // open method
            if (ModelState.IsValid)                         // check to see if our model is valid
            {                                               // open conditional
                context.Contents.Add(model.CurrentPaste);   // add the current item
                context.SaveChanges();                      // actually write it to the database
                return RedirectToAction("Index");           // take us to the index page
            }                                               // close conditional
            else                                            // modelstate is NOT valid
            {                                               // open else conditional
                model.Pastes = context.Contents.ToList();   // set the contents back
                return View(model);                         // return model
            }                                               // close else conditional
        }                                                   // close method
    }                                   // close our main home controller
}                                       // close our namespace
