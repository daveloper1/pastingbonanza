﻿using Microsoft.AspNetCore.Mvc; // use our mvc framework
using PastingBonanza.Models;    // grab our data models
using System.Linq;              // using database queries

/// <summary>
/// This controller is used for the paste submethods
/// </summary>
namespace PastingBonanza.Controllers    // use our namespace
{                                       // open our namespace
    public class PasteController : Controller   // this is our paste controller class
    {                                           // open our class
        private PasteContext context;           // create our data context
        public PasteController(PasteContext ctx)// make our constructor for data
        {                                       // open method
            context = ctx;                      // give it a name
        }                                       // close method

        /// <summary>
        /// If we get to this through other means, take us back to the index
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()                    // our index method
        {                                               // open method
            return RedirectToAction("Home", "Index");   // take us to the index page
        }                                               // close method

        [Route("Home/View/{id?}")]                      // view objects individually by index
        public IActionResult Paste(int id)              // our paste view method
        {                                               // open our method
            var pastes = context.Contents.OrderBy(c => c.Id).ToList();  // pull our table
            Paste paste = context.Contents.Find(id);                    // associate our object
            return View(paste);                                         // return our object
        }                                               // close our method
    }                                           // close our class
}                                       // close our namespace
