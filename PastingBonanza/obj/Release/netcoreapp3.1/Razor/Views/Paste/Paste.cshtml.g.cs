#pragma checksum "C:\Users\10104648\OneDrive\Documents\School\INFO3360\PastingBonanza\PastingBonanza\Views\Paste\Paste.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d44ed1734f74b9585a04ac3a54a717e602052c49"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Paste_Paste), @"mvc.1.0.view", @"/Views/Paste/Paste.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\10104648\OneDrive\Documents\School\INFO3360\PastingBonanza\PastingBonanza\Views\_ViewImports.cshtml"
using PastingBonanza;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\10104648\OneDrive\Documents\School\INFO3360\PastingBonanza\PastingBonanza\Views\_ViewImports.cshtml"
using PastingBonanza.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d44ed1734f74b9585a04ac3a54a717e602052c49", @"/Views/Paste/Paste.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c5726644d42b29d2d4974976f6f97ffdcde66ec0", @"/Views/_ViewImports.cshtml")]
    public class Views_Paste_Paste : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Paste>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\10104648\OneDrive\Documents\School\INFO3360\PastingBonanza\PastingBonanza\Views\Paste\Paste.cshtml"
  
    ViewBag.Title = "Paste Details";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Paste Details</h1>\r\n<div class=\"row\">\r\n    <div class=\"col-sm-8\">\r\n        <h2>Item: ");
#nullable restore
#line 9 "C:\Users\10104648\OneDrive\Documents\School\INFO3360\PastingBonanza\PastingBonanza\Views\Paste\Paste.cshtml"
             Write(Model.Label);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h2>\r\n        <table class=\"table table-bordered table-striped\">\r\n            <tr>\r\n                <td>Your Pasted Content:</td>\r\n            </tr>\r\n            <tr>\r\n                <td>");
#nullable restore
#line 15 "C:\Users\10104648\OneDrive\Documents\School\INFO3360\PastingBonanza\PastingBonanza\Views\Paste\Paste.cshtml"
               Write(Model.PasteItem);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n            </tr>\r\n        </table>\r\n    </div>\r\n</div>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Paste> Html { get; private set; }
    }
}
#pragma warning restore 1591
