﻿using System.Collections.Generic;   // make a data object for a view model

namespace PastingBonanza.Models     // our namespace
{                                   // open our namespace
    public class PasteViewModel     // create class for view model
    {                               // open class
        public PasteViewModel()                 // construct view model
        {                                       // open method
            CurrentPaste = new Paste();         // define our view model object
        }                                       // close method
        public List<Paste> Pastes { get; set; } // Pastes is a list of Paste
        public Paste CurrentPaste { get; set; } // define paste to model object
    }                               // close class
}                                   // close namespace
