﻿using System.ComponentModel.DataAnnotations;    // use annotations for errormessages

/// <summary>
/// This model is for our main database
/// quite simple
/// </summary>
namespace PastingBonanza.Models // our namespace
{                               // open our namespace
    public class Paste  // our database class
    {                   // open our database class
        public int Id { get; set; }             // the id attribute

        [Required(ErrorMessage = "Please enter a label, 25 characters or shorter.")]    // set our blank error message
        [MaxLength(25)]                                                                 // set our max length requirement
        public string Label { get; set; }       // the label attribute

        [Required(ErrorMessage = "Please paste an item up to 8000 characters.")]        // set our blank error message
        [MaxLength(8000)]                                                               // set our max length requirement
        public string PasteItem { get; set; }   // the paste item
    }                   // close our database class
}                               // close our namespace
