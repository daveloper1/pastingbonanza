﻿using Microsoft.EntityFrameworkCore;    // need this for our mvc model building

namespace PastingBonanza.Models         // our namespace
{                                       // open namespace
    public class PasteContext : DbContext   // our context class
    {                                       // open our context class
        public PasteContext(DbContextOptions<PasteContext> options) : base(options) { } // build our constructor to the database
        public DbSet<Paste> Contents { get; set; }                                      // make our db set

        protected override void OnModelCreating(ModelBuilder modelBuilder)  // our on-creating method used for our migrations
        {                                                                   // open our method
            modelBuilder.Entity<Paste>().HasData(                           // open our model builder
                new Paste { Id = 1, Label = "Sample 1", PasteItem = "This is a sample pasted content" },    // first data entry
                new Paste { Id = 2, Label = "Sample 2", PasteItem = "This is a sample pasted content2" },   // second data entry
                new Paste { Id = 3, Label = "Sample 3", PasteItem = "This is a sample pasted content3" }    // third data entry
            );                                                              // close our model building
        }                                                                   // close our method
    }                                       // close our context class
}                                       // close namespace
