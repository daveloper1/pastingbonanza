﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PastingBonanza.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contents",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Label = table.Column<string>(maxLength: 25, nullable: false),
                    PasteItem = table.Column<string>(maxLength: 8000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contents", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Contents",
                columns: new[] { "Id", "Label", "PasteItem" },
                values: new object[] { 1, "Sample 1", "This is a sample pasted content" });

            migrationBuilder.InsertData(
                table: "Contents",
                columns: new[] { "Id", "Label", "PasteItem" },
                values: new object[] { 2, "Sample 2", "This is a sample pasted content2" });

            migrationBuilder.InsertData(
                table: "Contents",
                columns: new[] { "Id", "Label", "PasteItem" },
                values: new object[] { 3, "Sample 3", "This is a sample pasted content3" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contents");
        }
    }
}
